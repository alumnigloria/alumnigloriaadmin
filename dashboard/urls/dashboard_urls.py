from django.urls import path
from ..views import dashboard_views as DashboardView


app_name = 'dashboard'

urlpatterns = [
    path('', DashboardView.index, name="index"),
    path('login', DashboardView.login, name="login"),
    path('signup', DashboardView.signup, name="signup"),
    path('tables', DashboardView.index, name='tables_index'),
    path('tables/<str:table_name>', DashboardView.tree_view, name='tree_view'),
    path('tables/<str:table_name>/<str:record_id>', DashboardView.form_view, name='form_view'),
    path('tables/<str:table_name>/<str:record_id>/<str:mode>', DashboardView.form_view, name='form_view_type'),
]
