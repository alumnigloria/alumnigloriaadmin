from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from datetime import datetime
import json
from tools import util, ERR
import locale
import logging, traceback
from alumni_gloria_admin import firebase
from django.conf import settings
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.encoders import encode_base64
from django.core.files.storage import FileSystemStorage


locale.setlocale(locale.LC_TIME, "en_US.UTF-8")
_logger = logging.getLogger("gloria_admin_logger")


@api_view(['GET', 'POST'])
def api_models(request):
    try:
        req_data = util.get_api_request_data(request)
        if req_data['action'] == 'login':
            res = login(request)
        elif req_data['action'] == 'logout':
            res = logout(request)
        elif req_data['action'] == 'signup':
            res = signup(request)
        elif req_data['action'] == 'add_record':
            res = add_record(request)
        elif req_data['action'] == 'update_record':
            res = update_record(request)
        elif req_data['action'] == 'delete_record':
            res = delete_record(request)
        elif req_data['action'] == 'delete_multi_records':
            res = delete_multi_records(request)
        elif req_data['action'] == 'duplicate_record':
            res = duplicate_record(request)
        elif req_data['action'] == 'send_email':
            res = send_email(request)
        else:
            res = ERR.get_error_api(1001)
    except Exception as e:
        res = ERR.get_error_api(500, additional_message=str(e))
    return Response(res)


def login(request):
    try:
        login_email = request._data.get('email', '')
        login_password = request._data.get('password', '')
        if not login_email:
            raise Exception('Please input email!')
        if not login_password:
            raise Exception('Please input password!')

        search_list = [
            ('email', '==', login_email),
            ('is_active', '==', True),
            ('is_approved', '==', True),
            ('is_staff', '==', True)
        ]
        response = firebase.search_data('users', search_list)
        if response.get('error_code'):
            return response
        else:
            if response.get('response'):
                auth_res = firebase.login(login_email, login_password)
                if not auth_res.get('error_code'):
                    found_user = response['response'][0]
                    request.session['logged_in_user'] = found_user
                    res = ERR.get_no_error(found_user)
                else:
                    res = ERR.get_error_api(500, additional_message="Invalid Email or Password")
            else:
                res = ERR.get_error_api(500, additional_message="Invalid Email or Password")
    except Exception as e:
        res = ERR.get_error_api(500)
        _logger.error(str(e) + '\n' + traceback.format_exc())
    return res


def logout(request):
    try:
        if request.session.get('logged_in_user'):
            del request.session['logged_in_user']
        res = ERR.get_no_error()
    except Exception as e:
        res = ERR.get_error_api(500, additional_message="Failed to Logout")
        _logger.error(str(e) + '\n' + traceback.format_exc())
    return res


def signup(request):
    try:
        user_dict = json.loads(request._data['data'])
        search_list = [
            ('email', '==', user_dict['email'])
        ]
        search_res = firebase.search_data('users', search_list)
        if search_res.get('error_code'):
            return search_res
        else:
            if search_res.get('response'):
                res = ERR.get_error_api(500, additional_message="User with that email is already exist!")
            else:
                auth_res = firebase.signup(user_dict['email'], user_dict['password'])
                if not auth_res.get('error_code'):
                    if user_dict.get('password'):
                        user_dict.pop('password')
                    response = firebase.add_data('users', user_dict)
                    if response.get('error_code'):
                        return response
                    res = ERR.get_no_error(response['response'])
                else:
                    res = ERR.get_error_api(500, additional_message="Signup Failed.")
    except Exception as e:
        res = ERR.get_error_api(500, additional_message=str(e))
        _logger.error(str(e) + '\n' + traceback.format_exc())
    return res


def add_record(request):
    try:
        data_dict = json.loads(request._data['data'])
        table_name = data_dict.pop('table_name')
        if len(request.FILES) != 0:
            fs = FileSystemStorage()
            fs.location += '/gallery/'
            for img in request.FILES:
                file = request.FILES[img]
                filename = fs.save(file.name, file)
                url = fs.location + file.name
                file_dict = {
                    'filename': '%s_%s_%s_%s_%s' % (datetime.now().strftime('%d%m%Y%H%M%S'), str(request.session['logged_in_user']['id']), table_name, img, file.name),
                    'filepath': url
                }
                data_dict.update({
                    img: file_dict
                })

        data_dict.update({
            'created_time': datetime.now().strftime('%d %b %Y'),
            'created_uid': request.session['logged_in_user']['id']
        })
        response = firebase.add_data(table_name, data_dict)
        if response.get('error_code'):
            return response
        res = ERR.get_no_error(response['response'])
    except Exception as e:
        res = ERR.get_error_api(500, additional_message=str(e))
        _logger.error(str(e) + '\n' + traceback.format_exc())
    return res


def duplicate_record(request):
    try:
        data_dict = json.loads(request._data['data'])
        table_name = data_dict.pop('table_name')
        record_id = data_dict.pop('record_id')

        get_response = firebase.get_data(table_name, record_id)
        if get_response.get('error_code'):
            return get_response
        result_dict = get_response['response']
        result_dict.pop('id')
        result_dict.update({
            'created_time': datetime.now().strftime('%d %b %Y'),
            'created_uid': request.session['logged_in_user']['id']
        })
        response = firebase.add_data(table_name, result_dict)
        if response.get('error_code'):
            return response
        res = ERR.get_no_error(response['response'])
    except Exception as e:
        res = ERR.get_error_api(500, additional_message=str(e))
        _logger.error(str(e) + '\n' + traceback.format_exc())
    return res


def update_record(request):
    try:
        data_dict = json.loads(request._data['data'])
        table_name = data_dict.pop('table_name')
        record_id = data_dict.pop('record_id')

        if len(request.FILES) != 0:
            fs = FileSystemStorage()
            fs.location += '/gallery/'
            for img in request.FILES:
                file = request.FILES[img]
                filename = fs.save(file.name, file)
                url = fs.location + file.name
                file_dict = {
                    'filename': '%s_%s_%s_%s_%s' % (datetime.now().strftime('%d%m%Y%H%M%S'), str(request.session['logged_in_user']['id']), table_name, img, file.name),
                    'filepath': url
                }
                data_dict.update({
                    img: file_dict
                })

        data_dict.update({
            'last_updated_time': datetime.now().strftime('%d %b %Y'),
            'last_updated_uid': request.session['logged_in_user']['id']
        })

        response = firebase.update_data(table_name, record_id, data_dict)
        if response.get('error_code'):
            return response
        res = ERR.get_no_error(response['response'])
    except Exception as e:
        res = ERR.get_error_api(500, additional_message=str(e))
        _logger.error(str(e) + '\n' + traceback.format_exc())
    return res


def delete_record(request):
    try:
        data_dict = json.loads(request._data['data'])
        table_name = data_dict.pop('table_name')
        record_id = data_dict.pop('record_id')

        response = firebase.delete_data(table_name, record_id)
        if response.get('error_code'):
            return response
        res = ERR.get_no_error(response['response'])
    except Exception as e:
        res = ERR.get_error_api(500, additional_message=str(e))
        _logger.error(str(e) + '\n' + traceback.format_exc())
    return res


def delete_multi_records(request):
    try:
        data_dict = json.loads(request._data['data'])
        table_name = data_dict.pop('table_name')
        record_id_list = data_dict.pop('record_id_list')

        deleted_rec_list = []
        for rec in json.loads(record_id_list):
            response = firebase.delete_data(table_name, rec)
            if response.get('error_code'):
                return response
            deleted_rec_list.append(response['response'])

        res = ERR.get_no_error(deleted_rec_list)
    except Exception as e:
        res = ERR.get_error_api(500, additional_message=str(e))
        _logger.error(str(e) + '\n' + traceback.format_exc())
    return res

def send_email(request):
    try:
        SMTP_USER = settings.SMTP_USER
        SMTP_PASS = settings.SMTP_PASS

        email_to = request._data.get('email_to', '')
        email_title = request._data.get('email_title', '')
        email_body = request._data.get('email_body', '')
        # Craft the email
        from_email = 'Alumni Gloria App <coklatjerman@gmail.com>'  # or simply the email address
        to_emails = [email_to]
        email_message = MIMEMultipart()
        email_message.add_header('To', ', '.join(to_emails))
        email_message.add_header('From', from_email)
        email_message.add_header('Subject', email_title)
        email_message.add_header('X-Priority', '1')

        html_txt = email_body

        body = MIMEText(html_txt, 'html')

        email_message.attach(body)

        # Connect, authenticate, and send mail
        smtp_server = smtplib.SMTP_SSL(settings.SMTP_HOST, port=smtplib.SMTP_SSL_PORT)
        smtp_server.set_debuglevel(1)  # Show SMTP server interactions
        smtp_server.login(SMTP_USER, SMTP_PASS)
        smtp_server.sendmail(from_email, to_emails, email_message.as_bytes())

        # Disconnect
        smtp_server.quit()
    except Exception as e:
        _logger.info('Failed to send email\n%s' % traceback.format_exc())
