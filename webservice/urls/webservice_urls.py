from django.urls import re_path
from ..views import webservice_views as WebserviceView


app_name = 'webservice'

urlpatterns = [
    re_path('', WebserviceView.api_models, name="webservice"),
]
