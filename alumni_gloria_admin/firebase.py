import time
import firebase_admin
import pyrebase
from datetime import timedelta
from uuid import uuid4
from firebase_admin import credentials, firestore, storage
from tools import util, ERR
import logging, traceback
from tools.db_tables import db_tables
from django.conf import settings

_logger = logging.getLogger("gloria_admin_logger")
cred = credentials.Certificate('gloria-alumni-app-export.json')
firebase_admin.initialize_app(cred, {
    'storageBucket': settings.STORAGE_BUCKET
})
pyre_config = {
    "apiKey": "AIzaSyDVebVeLyFk1gXAdWKmLSoYFVmvNZ9E5q4",
    "authDomain": "gloria-alumni-app.firebaseapp.com",
    "databaseURL": "https://gloria-alumni-app.firebaseio.com",
    "projectId": "gloria-alumni-app",
    "storageBucket": "gloria-alumni-app.appspot.com",
    "messagingSenderId": "697514563958",
    "appId": "1:697514563958:web:8ef95c3d4e53b97e108257",
    "measurementId": "G-7DG47YTDHL"
}
db = firestore.client()
bucket = storage.bucket()
pyre_obj = pyrebase.initialize_app(pyre_config)
auth = pyre_obj.auth()

def login(email, password):
    try:
        login_res = auth.sign_in_with_email_and_password(email, password)
        res = ERR.get_no_error(login_res)
    except Exception as e:
        res = ERR.get_error_api(500, additional_message="Failed to login.")
        _logger.error(str(e) + '\n' + traceback.format_exc())
    return res

def signup(email, password):
    try:
        signup_res = auth.create_user_with_email_and_password(email, password)
        res = ERR.get_no_error(signup_res)
    except Exception as e:
        res = ERR.get_error_api(500, additional_message="Failed to Signup.")
        _logger.error(str(e) + '\n' + traceback.format_exc())
    return res

def get_data(table, data_id=''):
    try:
        if data_id:
            data_ref_doc = db.collection(table).document(data_id)
            data_ref = data_ref_doc.get()
            if data_ref.exists:
                response = data_ref.to_dict()
                response.update({
                    'id': data_ref.id
                })
                for rec in db_tables:
                    if rec['name'] == table:
                        for rec2 in rec['fields']:
                            if rec2['type'] == 'many2one' and response.get(rec2['name']):
                                if isinstance(response[rec2['name']], str):
                                    rec2_split_str = response[rec2['name']].split('/')
                                    parent_table = rec2_split_str[0]
                                    parent_id = len(rec2_split_str) > 1 and rec2_split_str[1] or 'False'
                                else:
                                    parent_table = response[rec2['name']].parent.id
                                    parent_id = response[rec2['name']].id
                                many2one_data = db.collection(parent_table).document(parent_id)
                                many2one_ref = many2one_data.get()
                                if many2one_ref.exists:
                                    many2one_dict = many2one_ref.to_dict()
                                    if parent_table in ['users']:
                                        many2one_display = many2one_dict.get('first_name', '') + ' ' + many2one_dict.get('last_name', '')
                                    else:
                                        many2one_display = many2one_dict.get('name') and many2one_dict['name'] or parent_id
                                    final_dict = {
                                        'id': parent_id,
                                        'display': many2one_display
                                    }
                                    response.update({
                                        rec2['name']: final_dict
                                    })
            else:
                response = {}
        else:
            data_ref = db.collection(table).stream()
            response = []
            for data_rec in data_ref:
                data_dict = data_rec.to_dict()
                data_dict.update({
                    'id': data_rec.id
                })
                for rec in db_tables:
                    if rec['name'] == table:
                        for rec2 in rec['fields']:
                            if rec2['type'] == 'many2one' and data_dict.get(rec2['name']):
                                if isinstance(data_dict[rec2['name']], str):
                                    rec2_split_str = data_dict[rec2['name']].split('/')
                                    parent_table = rec2_split_str[0]
                                    parent_id = len(rec2_split_str) > 1 and rec2_split_str[1] or 'False'
                                else:
                                    parent_table = data_dict[rec2['name']].parent.id
                                    parent_id = data_dict[rec2['name']].id
                                many2one_data = db.collection(parent_table).document(parent_id)
                                many2one_ref = many2one_data.get()
                                if many2one_ref.exists:
                                    many2one_dict = many2one_ref.to_dict()
                                    if parent_table in ['users']:
                                        many2one_display = many2one_dict.get('first_name', '') + ' ' + many2one_dict.get('last_name', '')
                                    else:
                                        many2one_display = many2one_dict.get('name') and many2one_dict['name'] or parent_id
                                    final_dict = {
                                        'id': parent_id,
                                        'display': many2one_display
                                    }
                                    data_dict.update({
                                        rec2['name']: final_dict
                                    })
                response.append(data_dict)
        res = ERR.get_no_error(response)
    except Exception as e:
        res = ERR.get_error_api(500, additional_message="Failed to get data.")
        _logger.error(str(e) + '\n' + traceback.format_exc())
    return res


def search_data(table, search_tuple_list, is_new=False):
    try:
        data_ref = db.collection(table)
        for rec in search_tuple_list:
            if len(rec) > 3:
                if is_new:
                    search_ref_doc = '%s/%s' % (rec[3], rec[2])
                else:
                    search_ref_doc = db.collection(rec[3]).document(rec[2])
                data_ref = data_ref.where(rec[0], rec[1], search_ref_doc)
            else:
                data_ref = data_ref.where(rec[0], rec[1], rec[2])
        search_res = data_ref.stream()
        response = []
        for data_rec in search_res:
            data_dict = data_rec.to_dict()
            data_dict.update({
                'id': data_rec.id
            })
            for rec in db_tables:
                if rec['name'] == table:
                    for rec2 in rec['fields']:
                        if rec2['type'] == 'many2one' and data_dict.get(rec2['name']):
                            if isinstance(data_dict[rec2['name']], str):
                                rec2_split_str = data_dict[rec2['name']].split('/')
                                parent_table = rec2_split_str[0]
                                parent_id = len(rec2_split_str) > 1 and rec2_split_str[1] or 'False'
                            else:
                                parent_table = data_dict[rec2['name']].parent.id
                                parent_id = data_dict[rec2['name']].id
                            many2one_data = db.collection(parent_table).document(parent_id)
                            many2one_ref = many2one_data.get()
                            if many2one_ref.exists:
                                many2one_dict = many2one_ref.to_dict()
                                if parent_table in ['users']:
                                    many2one_display = many2one_dict.get('first_name', '') + ' ' + many2one_dict.get('last_name', '')
                                else:
                                    many2one_display = many2one_dict.get('name') and many2one_dict['name'] or parent_id
                                final_dict = {
                                    'id': parent_id,
                                    'display': many2one_display
                                }
                                data_dict.update({
                                    rec2['name']: final_dict
                                })
            response.append(data_dict)
        res = ERR.get_no_error(response)
    except Exception as e:
        res = ERR.get_error_api(500, additional_message="Failed to search data.")
        _logger.error(str(e) + '\n' + traceback.format_exc())
    return res


def upload_file(filename, filepath):
    try:
        blob = bucket.blob(filename)
        blob.upload_from_filename(filepath)
        res = ERR.get_no_error({
            'url': blob.public_url
        })
    except Exception as e:
        res = ERR.get_error_api(500, additional_message="Failed to upload file.")
        _logger.error(str(e) + '\n' + traceback.format_exc())
    return res


def add_data(table, data):
    try:
        data_ref_doc = db.collection(table).document()

        update_dict = {}
        for rec in db_tables:
            if rec['name'] == table:
                for rec2 in rec['fields']:
                    for key, val in data.items():
                        if key == rec2['name']:
                            if rec2['type'] == 'many2one':
                                if val:
                                    # update_dict.update({
                                    #     key: db.collection(rec2['related_table']).document(val)
                                    # })
                                    update_dict.update({
                                        key: str(rec2['related_table']) + '/' + str(val)
                                    })
                            elif rec2['type'] == 'boolean':
                                if val:
                                    update_dict.update({
                                        key: bool(val)
                                    })
                            elif rec2['type'] == 'int':
                                if val:
                                    update_dict.update({
                                        key: int(val)
                                    })
                            elif rec2['type'] == 'float':
                                if val:
                                    update_dict.update({
                                        key: float(val)
                                    })
                            elif rec2['type'] == 'image':
                                if val:
                                    upload_res = upload_file(val['filename'], val['filepath'])
                                    if upload_res.get('error_code'):
                                        _logger.error('Failed to upload image file: %s' % (key,))
                                    else:
                                        update_dict.update({
                                            key: upload_res['response']['url']
                                        })
                            else:
                                if val:
                                    update_dict.update({
                                        key: str(val)
                                    })
                    if rec2.get('default_value') and not data.get(rec2['name']):
                        update_dict.update({
                            rec2['name']: rec2['default_value']
                        })

        if table in ['users', 'businesses'] and data.get('password'):
            update_dict.update({
                'password': util.get_encrypted_password(data['password'])
            })

        data.update(update_dict)
        data_ref_doc.set(data)
        data_ref = data_ref_doc.get()
        response = get_data(table, data_ref.id)
        if response.get('error_code'):
            return response
        response = response['response']
        res = ERR.get_no_error(response)
    except Exception as e:
        res = ERR.get_error_api(500, additional_message="Failed to add data.")
        _logger.error(str(e) + '\n' + traceback.format_exc())
    return res


def update_data(table, data_id, data):
    try:
        data_ref_doc = db.collection(table).document(data_id)
        data_ref = data_ref_doc.get()
        if data_ref.exists:
            update_dict = {}
            for rec in db_tables:
                if rec['name'] == table:
                    for rec2 in rec['fields']:
                        for key, val in data.items():
                            if key == rec2['name']:
                                if rec2['type'] == 'many2one':
                                    if val:
                                        # update_dict.update({
                                        #     key: db.collection(rec2['related_table']).document(val)
                                        # })
                                        update_dict.update({
                                            key: str(rec2['related_table']) + '/' + str(val)
                                        })
                                elif rec2['type'] == 'boolean':
                                    if val:
                                        update_dict.update({
                                            key: bool(val)
                                        })
                                elif rec2['type'] == 'int':
                                    if val:
                                        update_dict.update({
                                            key: int(val)
                                        })
                                elif rec2['type'] == 'float':
                                    if val:
                                        update_dict.update({
                                            key: float(val)
                                        })
                                elif rec2['type'] == 'image':
                                    if val:
                                        upload_res = upload_file(val['filename'], val['filepath'])
                                        if upload_res.get('error_code'):
                                            _logger.error('Failed to upload image file: %s' % (key,))
                                        else:
                                            update_dict.update({
                                                key: upload_res['response']['url']
                                            })
                                else:
                                    if val:
                                        update_dict.update({
                                            key: str(val)
                                        })

            if table in ['users', 'businesses'] and data.get('password'):
                update_dict.update({
                    'password': util.get_encrypted_password(data['password'])
                })

            data.update(update_dict)
            data_ref_doc.update(data)
        else:
            raise Exception('Data does not exist!')
        response = get_data(table, data_id)
        if response.get('error_code'):
            return response
        response = response['response']
        res = ERR.get_no_error(response)
    except Exception as e:
        res = ERR.get_error_api(500, additional_message="Failed to update data.")
        _logger.error(str(e) + '\n' + traceback.format_exc())
    return res


def delete_data(table, data_id):
    try:
        data_ref_doc = db.collection(table).document(data_id)
        data_ref = data_ref_doc.get()
        if data_ref.exists:
            response = get_data(table, data_id)
            if response.get('error_code'):
                return response
            response = response['response']
            data_ref_doc.delete()
        else:
            raise Exception('Data does not exist!')
        res = ERR.get_no_error(response)
    except Exception as e:
        res = ERR.get_error_api(500, additional_message="Failed to update data.")
        _logger.error(str(e) + '\n' + traceback.format_exc())
    return res
