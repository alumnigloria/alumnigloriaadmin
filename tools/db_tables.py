# access: self (self owned only), public (everyone), staff (staff only)
db_tables = [
    {
        'name': 'users',
        'display': 'Users',
        'view_access': 'self',
        'create_access': 'staff',
        'edit_access': 'staff',
        'delete_access': 'staff',
        'fields': [
            {
                'name': 'first_name',
                'display': 'First Name',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'last_name',
                'display': 'Last Name',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'email',
                'display': 'Email',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'contact_number',
                'display': 'Contact Number',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'birth_date',
                'display': 'Birth Date',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'date',
                'default_value': ''
            },
            {
                'name': 'year_of_graduation',
                'display': 'Year of Graduation',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'int',
                'default_value': 2019
            },
            {
                'name': 'biography',
                'display': 'Biography',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'text',
                'default_value': ''
            },
            {
                'name': 'facebook_url',
                'display': 'Facebook URL',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'instagram',
                'display': 'Instagram',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'linkedin',
                'display': 'LinkedIN',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'address',
                'display': 'Address',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'text',
                'default_value': ''
            },
            {
                'name': 'location',
                'display': 'Location',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'profile_picture',
                'display': 'Profile Picture',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'image',
                'default_value': ''
            },
            {
                'name': 'current_occupation',
                'display': 'Current Occupation',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'sponsorship',
                'display': 'Sponsorship',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'selection',
                'options': [('none', 'None'), ('package_1', 'Package 1'), ('package_2', 'Package 2'), ('package_3', 'Package 3')],
                'default_value': 'none'
            },
            {
                'name': 'sponsor_order',
                'display': 'Sponsor Order',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'int',
                'default_value': 0
            },
            {
                'name': 'following_ids',
                'display': 'Followings',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': False,
                'type': 'one2many',
                'tree_fields': [('second_user_id', 'Second User'), ('connected_date', 'Connected Date')],
                'related_table': 'user_connections',
                'related_field': 'first_user_id',
                'default_value': []
            },
            {
                'name': 'follower_ids',
                'display': 'Followers',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': False,
                'type': 'one2many',
                'tree_fields': [('first_user_id', 'First User'), ('connected_date', 'Connected Date')],
                'related_table': 'user_connections',
                'related_field': 'second_user_id',
                'default_value': []
            },
            {
                'name': 'business_ids',
                'display': 'Businesses',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': False,
                'type': 'one2many',
                'tree_fields': [('business_id', 'Business'), ('position', 'Position'), ('join_date', 'Join Date'), ('leave_date', 'Leave Date'), ('is_active', 'Is Active')],
                'related_table': 'users_businesses_rel',
                'related_field': 'user_id',
                'default_value': []
            },
            {
                'name': 'promotion_ids',
                'display': 'Promotions',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': False,
                'type': 'one2many',
                'tree_fields': [('name', 'Name'), ('start_time', 'Start Time'), ('end_time', 'End Time'), ('location', 'Location'), ('is_active', 'Is Active')],
                'related_table': 'promotions',
                'related_field': 'user_owner_id',
                'default_value': []
            },
            {
                'name': 'communities_ids',
                'display': 'Communities',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': False,
                'type': 'one2many',
                'tree_fields': [('name', 'Name'), ('location', 'Location'), ('is_active', 'Is Active')],
                'related_table': 'communities',
                'related_field': 'user_owner_id',
                'default_value': []
            },
            {
                'name': 'last_seen',
                'display': 'Last Seen',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': False,
                'type': 'datetime',
                'default_value': ''
            },
            {
                'name': 'is_active',
                'display': 'Is Active',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': True,
                'type': 'boolean',
                'default_value': True
            },
            {
                'name': 'is_approved',
                'display': 'Is Approved',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': True,
                'type': 'boolean',
                'default_value': False
            },
            {
                'name': 'is_staff',
                'display': 'Is Staff',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': True,
                'type': 'boolean',
                'default_value': False
            },
            {
                'name': 'is_super_user',
                'display': 'Is Super User',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': True,
                'type': 'boolean',
                'default_value': False
            },
            {
                'name': 'profile_preference',
                'display': 'Profile Preference',
                'required': True,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'selection',
                'options': [('public', 'Public'), ('connection', 'Connections Only'), ('private', 'Private')],
                'default_value': 'public'
            },
            {
                'name': 'created_time',
                'display': 'Created Time',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'datetime',
                'default_value': ''
            },
            {
                'name': 'created_uid',
                'display': 'Created By',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'users',
                'options': [],
                'default_value': False
            },
            {
                'name': 'last_updated_time',
                'display': 'Last Updated Time',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'datetime',
                'default_value': ''
            },
            {
                'name': 'last_updated_uid',
                'display': 'Last Updated By',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'users',
                'options': [],
                'default_value': False
            }
        ]
    },
    {
        'name': 'user_connections',
        'display': 'User Connections',
        'view_access': 'staff',
        'create_access': 'staff',
        'edit_access': 'staff',
        'delete_access': 'staff',
        'fields': [
            {
                'name': 'first_user_id',
                'display': 'First User',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'users',
                'options': [],
                'default_value': False
            },
            {
                'name': 'second_user_id',
                'display': 'Second User',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'users',
                'options': [],
                'default_value': False
            },
            {
                'name': 'connected_date',
                'display': 'Connected Date',
                'required': True,
                'readonly': True,
                'tree_view': True,
                'staff_only': True,
                'type': 'date',
                'default_value': ''
            }
        ]
    },
    {
        'name': 'businesses',
        'display': 'Businesses',
        'view_access': 'self',
        'create_access': 'staff',
        'edit_access': 'staff',
        'delete_access': 'staff',
        'fields': [
            {
                'name': 'name',
                'display': 'Name',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'email',
                'display': 'Email',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'contact_number',
                'display': 'Contact Number',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'established_year',
                'display': 'Established Year',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'int',
                'default_value': 2020
            },
            {
                'name': 'address',
                'display': 'Address',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'text',
                'default_value': ''
            },
            {
                'name': 'location',
                'display': 'Location',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'description',
                'display': 'Description',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'text',
                'default_value': ''
            },
            {
                'name': 'facebook_url',
                'display': 'Facebook URL',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'instagram',
                'display': 'Instagram',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'linkedin',
                'display': 'LinkedIN',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'business_url',
                'display': 'Business URL',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'business_logo',
                'display': 'Business Logo',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'image',
                'default_value': ''
            },
            {
                'name': 'business_type_ids',
                'display': 'Business Types',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': False,
                'type': 'one2many',
                'tree_fields': [('business_type_id', 'Business Type')],
                'related_table': 'businesses_business_types_rel',
                'related_field': 'business_id',
                'default_value': []
            },
            {
                'name': 'user_ids',
                'display': 'Users',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': False,
                'type': 'one2many',
                'tree_fields': [('user_id', 'User'), ('position', 'Position'), ('join_date', 'Join Date'), ('leave_date', 'Leave Date'), ('business_profile_access', 'Business Profile Access'), ('is_active', 'Is Active')],
                'related_table': 'users_businesses_rel',
                'related_field': 'business_id',
                'default_value': []
            },
            {
                'name': 'promotion_ids',
                'display': 'Promotions',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': False,
                'type': 'one2many',
                'tree_fields': [('name', 'Name'), ('start_time', 'Start Time'), ('end_time', 'End Time'),
                                ('location', 'Location'), ('is_active', 'Is Active')],
                'related_table': 'promotions',
                'related_field': 'business_owner_id',
                'default_value': []
            },
            {
                'name': 'communities_ids',
                'display': 'Communities',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': False,
                'type': 'one2many',
                'tree_fields': [('name', 'Name'), ('location', 'Location'), ('is_active', 'Is Active')],
                'related_table': 'communities',
                'related_field': 'business_owner_id',
                'default_value': []
            },
            {
                'name': 'image_ids',
                'display': 'Images',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': False,
                'type': 'one2many',
                'tree_fields': [('name', 'Name')],
                'related_table': 'images',
                'related_field': 'business_id',
                'default_value': []
            },
            {
                'name': 'last_seen',
                'display': 'Last Seen',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': False,
                'type': 'datetime',
                'default_value': ''
            },
            {
                'name': 'sponsorship',
                'display': 'Sponsorship',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'selection',
                'options': [('none', 'None'), ('package_1', 'Package 1'), ('package_2', 'Package 2'), ('package_3', 'Package 3')],
                'default_value': 'none'
            },
            {
                'name': 'sponsor_order',
                'display': 'Sponsor Order',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'int',
                'default_value': 0
            },
            {
                'name': 'is_active',
                'display': 'Is Active',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': True,
                'type': 'boolean',
                'default_value': True
            },
            {
                'name': 'is_approved',
                'display': 'Is Approved',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': True,
                'type': 'boolean',
                'default_value': False
            },
            {
                'name': 'business_profile_preference',
                'display': 'Business Profile Preference',
                'required': True,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'selection',
                'options': [('public', 'Public'), ('private', 'Private')],
                'default_value': 'public'
            },
            {
                'name': 'created_time',
                'display': 'Created Time',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'datetime',
                'default_value': ''
            },
            {
                'name': 'created_uid',
                'display': 'Created By',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'users',
                'options': [],
                'default_value': False
            },
            {
                'name': 'last_updated_time',
                'display': 'Last Updated Time',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'datetime',
                'default_value': ''
            },
            {
                'name': 'last_updated_uid',
                'display': 'Last Updated By',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'users',
                'options': [],
                'default_value': False
            }
        ]
    },
    {
        'name': 'users_businesses_rel',
        'display': 'Users - Business Relation',
        'view_access': 'self',
        'create_access': 'staff',
        'edit_access': 'staff',
        'delete_access': 'staff',
        'fields': [
            {
                'name': 'user_id',
                'display': 'User',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'users',
                'options': [],
                'default_value': False
            },
            {
                'name': 'business_id',
                'display': 'Business',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'businesses',
                'options': [],
                'default_value': False
            },
            {
                'name': 'position',
                'display': 'Position',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'join_date',
                'display': 'Join Date',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'date',
                'default_value': ''
            },
            {
                'name': 'leave_date',
                'display': 'Leave Date',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'date',
                'default_value': ''
            },
            {
                'name': 'is_active',
                'display': 'Is Active',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'boolean',
                'default_value': True
            },
            {
                'name': 'is_approved_user',
                'display': 'Is Approved by User',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'boolean',
                'default_value': True
            },
            {
                'name': 'is_approved_business',
                'display': 'Is Approved by Business',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'boolean',
                'default_value': True
            },
            {
                'name': 'business_profile_access',
                'display': 'Can Edit Business Profile',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'boolean',
                'default_value': False
            },
            {
                'name': 'last_updated_time',
                'display': 'Last Updated Time',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'datetime',
                'default_value': ''
            },
            {
                'name': 'last_updated_uid',
                'display': 'Last Updated By',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'users',
                'options': [],
                'default_value': False
            }
        ]
    },
    {
        'name': 'business_types',
        'display': 'Business Types',
        'view_access': 'public',
        'create_access': 'staff',
        'edit_access': 'staff',
        'delete_access': 'staff',
        'fields': [
            {
                'name': 'name',
                'display': 'Name',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'description',
                'display': 'Description',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'text',
                'default_value': ''
            },
            {
                'name': 'business_ids',
                'display': 'Businesses',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': False,
                'type': 'one2many',
                'tree_fields': [('business_id', 'Business')],
                'related_table': 'businesses_business_types_rel',
                'related_field': 'business_type_id',
                'default_value': []
            },
            {
                'name': 'created_time',
                'display': 'Created Time',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'datetime',
                'default_value': ''
            },
            {
                'name': 'created_uid',
                'display': 'Created By',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'users',
                'options': [],
                'default_value': False
            },
            {
                'name': 'last_updated_time',
                'display': 'Last Updated Time',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'datetime',
                'default_value': ''
            },
            {
                'name': 'last_updated_uid',
                'display': 'Last Updated By',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'users',
                'options': [],
                'default_value': False
            }
        ]
    },
    {
        'name': 'businesses_business_types_rel',
        'display': 'Business - Business Type Relation',
        'view_access': 'staff',
        'create_access': 'staff',
        'edit_access': 'staff',
        'delete_access': 'staff',
        'fields': [
            {
                'name': 'business_id',
                'display': 'Business',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'businesses',
                'options': [],
                'default_value': False
            },
            {
                'name': 'business_type_id',
                'display': 'Business Type',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'business_types',
                'options': [],
                'default_value': False
            }
        ]
    },
    {
        'name': 'promotions',
        'display': 'Promotions',
        'view_access': 'self',
        'create_access': 'staff',
        'edit_access': 'staff',
        'delete_access': 'staff',
        'fields': [
            {
                'name': 'name',
                'display': 'Name',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'description',
                'display': 'Description',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'text',
                'default_value': ''
            },
            {
                'name': 'type',
                'display': 'Type',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'selection',
                'options': [('product', 'Product'), ('event', 'Event'), ('others', 'Others')],
                'default_value': 'product'
            },
            {
                'name': 'contact',
                'display': 'Contact',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'user_owner_id',
                'display': 'Owner (User)',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'many2one',
                'related_table': 'users',
                'options': [],
                'default_value': False
            },
            {
                'name': 'business_owner_id',
                'display': 'Owner (Business)',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'many2one',
                'related_table': 'businesses',
                'options': [],
                'default_value': False
            },
            {
                'name': 'start_time',
                'display': 'Start Time',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'datetime',
                'default_value': ''
            },
            {
                'name': 'end_time',
                'display': 'End Time',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'datetime',
                'default_value': ''
            },
            {
                'name': 'location',
                'display': 'Location',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'promotion_url',
                'display': 'Promotion URL',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'image_ids',
                'display': 'Images',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'one2many',
                'tree_fields': [('name', 'Name')],
                'related_table': 'images',
                'related_field': 'promotion_id',
                'default_value': []
            },
            {
                'name': 'is_active',
                'display': 'Is Active',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'boolean',
                'default_value': ''
            },
            {
                'name': 'is_approved',
                'display': 'Is Approved',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': True,
                'type': 'boolean',
                'default_value': ''
            },
            {
                'name': 'is_featured',
                'display': 'Is Featured',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': True,
                'type': 'boolean',
                'default_value': ''
            },
            {
                'name': 'sequence',
                'display': 'Sequence',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': True,
                'type': 'int',
                'default_value': 0
            },
            {
                'name': 'created_time',
                'display': 'Created Time',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'datetime',
                'default_value': ''
            },
            {
                'name': 'created_uid',
                'display': 'Created By',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'users',
                'options': [],
                'default_value': False
            },
            {
                'name': 'last_updated_time',
                'display': 'Last Updated Time',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'datetime',
                'default_value': ''
            },
            {
                'name': 'last_updated_uid',
                'display': 'Last Updated By',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'users',
                'options': [],
                'default_value': False
            }
        ]
    },
    {
        'name': 'communities',
        'display': 'Communities',
        'view_access': 'self',
        'create_access': 'staff',
        'edit_access': 'staff',
        'delete_access': 'staff',
        'fields': [
            {
                'name': 'name',
                'display': 'Name',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'description',
                'display': 'Description',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'text',
                'default_value': ''
            },
            {
                'name': 'contact',
                'display': 'Contact',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'user_owner_id',
                'display': 'Owner (User)',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'many2one',
                'related_table': 'users',
                'options': [],
                'default_value': False
            },
            {
                'name': 'business_owner_id',
                'display': 'Owner (Business)',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'many2one',
                'related_table': 'businesses',
                'options': [],
                'default_value': False
            },
            {
                'name': 'location',
                'display': 'Location',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'community_url',
                'display': 'Community URL',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'image_ids',
                'display': 'Images',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'one2many',
                'tree_fields': [('name', 'Name')],
                'related_table': 'images',
                'related_field': 'community_id',
                'default_value': []
            },
            {
                'name': 'is_active',
                'display': 'Is Active',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'boolean',
                'default_value': ''
            },
            {
                'name': 'is_approved',
                'display': 'Is Approved',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': True,
                'type': 'boolean',
                'default_value': ''
            },
            {
                'name': 'is_featured',
                'display': 'Is Featured',
                'required': False,
                'readonly': False,
                'tree_view': False,
                'staff_only': True,
                'type': 'boolean',
                'default_value': ''
            },
            {
                'name': 'sequence',
                'display': 'Sequence',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': True,
                'type': 'int',
                'default_value': 0
            },
            {
                'name': 'created_time',
                'display': 'Created Time',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'datetime',
                'default_value': ''
            },
            {
                'name': 'created_uid',
                'display': 'Created By',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'users',
                'options': [],
                'default_value': False
            },
            {
                'name': 'last_updated_time',
                'display': 'Last Updated Time',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'datetime',
                'default_value': ''
            },
            {
                'name': 'last_updated_uid',
                'display': 'Last Updated By',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'users',
                'options': [],
                'default_value': False
            }
        ]
    },
    {
        'name': 'images',
        'display': 'Images',
        'view_access': 'self',
        'create_access': 'staff',
        'edit_access': 'staff',
        'delete_access': 'staff',
        'fields': [
            {
                'name': 'name',
                'display': 'Name',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': False,
                'type': 'char',
                'default_value': ''
            },
            {
                'name': 'image',
                'display': 'Image',
                'required': True,
                'readonly': False,
                'tree_view': False,
                'staff_only': False,
                'type': 'image',
                'default_value': ''
            },
            {
                'name': 'community_id',
                'display': 'Community',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'communities',
                'options': [],
                'default_value': False
            },
            {
                'name': 'promotion_id',
                'display': 'Promotion',
                'required': False,
                'readonly': False,
                'tree_view': True,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'promotions',
                'options': [],
                'default_value': False
            },
            {
                'name': 'sequence',
                'display': 'Sequence',
                'required': True,
                'readonly': False,
                'tree_view': True,
                'staff_only': True,
                'type': 'int',
                'default_value': 0
            },
            {
                'name': 'created_time',
                'display': 'Created Time',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'datetime',
                'default_value': ''
            },
            {
                'name': 'created_uid',
                'display': 'Created By',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'users',
                'options': [],
                'default_value': False
            },
            {
                'name': 'last_updated_time',
                'display': 'Last Updated Time',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'datetime',
                'default_value': ''
            },
            {
                'name': 'last_updated_uid',
                'display': 'Last Updated By',
                'required': False,
                'readonly': True,
                'tree_view': False,
                'staff_only': True,
                'type': 'many2one',
                'related_table': 'users',
                'options': [],
                'default_value': False
            }
        ]
    }
]
